# lua-cmake

CMake file for building Lua. Download Lua source at https://www.lua.org/ftp/, extract the files and place CMakeLists.txt into the root directory.

Generated project tested only with Lua 5.4.3, Windows, Visual Studio 2019.
