cmake_minimum_required(VERSION 3.19)

project(Lua C)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/build)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${PROJECT_SOURCE_DIR}/build/debug)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${PROJECT_SOURCE_DIR}/build/release)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O2 -W3 -DLUA_COMPAT_5_3 -DLUA_BUILD_AS_DLL")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -W3 -DLUA_COMPAT_5_3 -DLUA_BUILD_AS_DLL")

set(CoreSrcPath ${PROJECT_SOURCE_DIR}/src)

set(SOURCE_FILES_C
	${CoreSrcPath}/lapi.c
	${CoreSrcPath}/lauxlib.c
	${CoreSrcPath}/lbaselib.c
	${CoreSrcPath}/lcode.c
	${CoreSrcPath}/lcorolib.c
	${CoreSrcPath}/lctype.c
	${CoreSrcPath}/ldblib.c
	${CoreSrcPath}/ldebug.c
	${CoreSrcPath}/ldo.c
	${CoreSrcPath}/ldump.c
	${CoreSrcPath}/lfunc.c
	${CoreSrcPath}/lgc.c
	${CoreSrcPath}/linit.c
	${CoreSrcPath}/liolib.c
	${CoreSrcPath}/llex.c
	${CoreSrcPath}/lmathlib.c
	${CoreSrcPath}/lmem.c
	${CoreSrcPath}/loadlib.c
	${CoreSrcPath}/lobject.c
	${CoreSrcPath}/lopcodes.c
	${CoreSrcPath}/loslib.c
	${CoreSrcPath}/lparser.c
	${CoreSrcPath}/lstate.c
	${CoreSrcPath}/lstring.c
	${CoreSrcPath}/lstrlib.c
	${CoreSrcPath}/ltable.c
	${CoreSrcPath}/ltablib.c
	${CoreSrcPath}/ltm.c
	${CoreSrcPath}/lundump.c
	${CoreSrcPath}/lutf8lib.c
	${CoreSrcPath}/lvm.c
	${CoreSrcPath}/lzio.c)

set(SOURCE_FILES_H
	${CoreSrcPath}/lapi.h
	${CoreSrcPath}/lcode.h
	${CoreSrcPath}/lctype.h
	${CoreSrcPath}/ldebug.h
	${CoreSrcPath}/ldo.h
	${CoreSrcPath}/lfunc.h
	${CoreSrcPath}/lgc.h
	${CoreSrcPath}/ljumptab.h
	${CoreSrcPath}/llex.h
	${CoreSrcPath}/llimits.h
	${CoreSrcPath}/lmem.h
	${CoreSrcPath}/lobject.h
	${CoreSrcPath}/lopcodes.h
	${CoreSrcPath}/lopnames.h
	${CoreSrcPath}/lparser.h
	${CoreSrcPath}/lprefix.h
	${CoreSrcPath}/lstate.h
	${CoreSrcPath}/lstring.h
	${CoreSrcPath}/ltable.h
	${CoreSrcPath}/ltm.h
	${CoreSrcPath}/lundump.h
	${CoreSrcPath}/lvm.h
	${CoreSrcPath}/lzio.h)

set(SOURCE_FILES_PUBLIC_H
	${CoreSrcPath}/lua.h
	${CoreSrcPath}/luaconf.h
	${CoreSrcPath}/lualib.h
	${CoreSrcPath}/lauxlib.h
	${CoreSrcPath}/lua.hpp)

set(SOURCE_FILES_LIBRARY ${SOURCE_FILES_C} ${SOURCE_FILES_H} ${SOURCE_FILES_PUBLIC_H})

set(SOURCE_FILES_LUA
	${SOURCE_FILES_H}
	${CoreSrcPath}/lua.c)

set(SOURCE_FILES_LUAC
	${SOURCE_FILES_LIBRARY}
	${CoreSrcPath}/luac.c)

add_library(lua54 SHARED ${SOURCE_FILES_LIBRARY})
target_include_directories(lua54 PRIVATE ${CoreSrcPath})
set_target_properties(lua54 PROPERTIES
	PUBLIC_HEADER "${SOURCE_FILES_PUBLIC_H}")

add_executable(lua ${SOURCE_FILES_LUA})
target_include_directories(lua PRIVATE ${CoreSrcPath})
target_link_libraries(lua lua54)

add_executable(luac ${SOURCE_FILES_LUAC})
target_include_directories(luac PRIVATE ${CoreSrcPath})
#target_link_libraries(luac lua54)

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
	set(CMAKE_INSTALL_PREFIX "" CACHE PATH "Lua installation directory" FORCE)
endif()

if (CMAKE_INSTALL_PREFIX)
	install(TARGETS lua54 lua luac
			ARCHIVE DESTINATION lib
	        LIBRARY DESTINATION lib
	        PUBLIC_HEADER DESTINATION include
	        RUNTIME DESTINATION bin
	)
endif()